import numpy as np
import math
import torch
import torch.nn.functional as F
#from sklearn.preprocessing import StandardScaler
import random

class TradingAgent:
    def reward_function(self, history):
        # TODO feel free to change the reward function ...
        #  This is the default one used in the gym-trading-env library, however, there might be better ones
        #  @see https://gym-trading-env.readthedocs.io/en/latest/customization.html#custom-reward-function
        return np.log(history["portfolio_valuation", -1] / history["portfolio_valuation", -2])

    def make_features(self, df):
        # TODO feel free to include your own features - for example, Bollinger bounds
        #  IMPORTANT - do not create features that look ahead in time.
        #  Doing so will result in 0 points from the homework.
        #  @see https://gym-trading-env.readthedocs.io/en/latest/features.html

        # Create the feature : ( close[t] - close[t-1] )/ close[t-1]
        # this is percentual change in time


        #df["feature_reward"] = (df["close"].pct_change())

        df["feature_close"] = df["close"].pct_change()
        df["feature_close_sum_7"] = df["feature_close"].rolling(3*24).sum()
        df["feature_close_sum_24"] = df["feature_close"].rolling(7*24).sum()
        #df['feature_bollinger'] = (df['close'] - df['close'].rolling(20*24).mean()) / (2 * df['close'].rolling(20*24).std())
        #df["feature_volatility"] = df["close"].rolling(7*24).std()


        """#moving average
        df['feature_ma_7'] = df['close'].rolling(3*24).mean()

        #feature mac
        df['feature_macd'] = df['close'].ewm(span=12*24, adjust=False).mean() - df['close'].ewm(span=26*24, adjust=False).mean()
        """

        # Create the feature : close[t] / open[t]
        df["feature_open"] = df["close"] / df["open"]
        # Create the feature : high[t] / close[t]
        df["feature_high"] = df["high"] / df["close"]
        # Create the feature : low[t] / close[t]
        df["feature_low"] = df["low"] / df["close"]
        # Create the feature : volume[t] / max(*volume[t-7*24:t+1])
        df["feature_volume"] = df["volume"] / df["volume"].rolling(7 * 24).max()
        df.dropna(inplace=True)    

        #add bollinger bonds




        #total feaure close over last few days to determine if market has been in up or down in general

        # the library automatically adds two features - your position and

        return df

    def get_position_list(self):
        # TODO feel free to specify different set of actions
        #  here, the acceptable actions are positions -1.0, -0.9, ..., 2.0
        #  corresponding actions are integers 0, 1, ..., 30
        #  @see https://gym-trading-env.readthedocs.io/en/latest/environment_desc.html#action-space
        #return [x / 10.0 for x in range(-10, 21)]
        return [-1, 0, 1]
        #return [-1, 0]
        #return [0, 1]

    def initialize(self, n_actions, n_states, learning_rate=0.1, gamma=0, epsilon=0.5, epsilon_min=0.01, epsilon_decay=0.995):
        self.n_actions = n_actions
        self.n_states = n_states
        #self.q_table = np.zeros((n_states, n_actions))
        #i want to initialize the q_table with random values
        self.q_table = np.zeros((n_states, n_actions))
        self.q_table_visits = np.zeros((n_states, n_actions))
        self.learning_rate = learning_rate
        self.gamma = gamma
        self.epsilon = epsilon
        self.epsilon_max = epsilon
        self.epsilon_min = epsilon_min
        self.epsilon_decay = epsilon_decay

    def get_current_state(self, observation):
        # Example state categorization logic based on price movement and volume
        

        state = 0
        
        if(observation[0] > 0.05 or observation[0] < -0.04):
            state += 2  
        if(observation[0] > 0.01):
            state += 4
        elif(observation[0] < -0.01):
            state += 8

        if(observation[1] > 0.01):
            state += 16
        elif(observation[1] < -0.01):
            state += 32

        """if(observation[2] > 0.01):
            state += 64
        elif(observation[2] < -0.01):
            state += 128"""

        #position:
        """if(observation[len(observation)-1] > 0.5):
            state += 64
        elif(observation[len(observation)-1] < -0.5):
            state += 128
        else:
            state += 256"""

        #q: there are bollinger bonds, what should be their range?
        #a: 
        """if(observation[3]  > 0.7):
            state += 512
        elif(observation[3] < 0.3):
            state += 1024
        else:
            state += 2048

        if(observation[4]  > 0):
            state += 4096
        else:
            state += 8192"""





        """for i in range(len(observation)):
            if observation[i] > 0:
                state+=+2**i
        #print("state", state)"""

        
        
        return state

    def select_action(self, state):
        if random.uniform(0, 1) < self.epsilon:
            return random.randint(0, self.n_actions - 1)
        else:
            #return np.argmax(self.q_table[state])
            #select one randomly if multiple are max
            return np.random.choice(np.where(self.q_table[state] == self.q_table[state].max())[0])

    def update_q_table(self, state, action, reward, next_state):
        #converted_state = self.get_current_state(state)
        #converted_next_state = self.get_current_state(next_state)

        #print("info of update: ", state, action, reward, next_state)


        converted_state = state
        converted_next_state = next_state

        self.q_table_visits[converted_state, action] += 1


        next_max = np.max(self.q_table[converted_next_state])
        #self.q_table[converted_state, action] = self.q_table[converted_state, action] + \
        #                              self.learning_rate * (reward + self.gamma * next_max - self.q_table[converted_state, action])
        
        
        #self.q_table[converted_state, action] = self.q_table[converted_state, action] + reward

        #self.q_table[converted_state, action] = self.q_table[converted_state, action] + 1/self.q_table_visits[converted_state, action] * (reward - self.q_table[converted_state, action])

        self.q_table[converted_state, action] = (self.q_table[converted_state, action]*(self.q_table_visits[converted_state, action]-1) + reward)/(self.q_table_visits[converted_state, action])

        #self.q_table[converted_state, action] = self.q_table[converted_state, 2-action] - reward

    def update_epsilon(self):
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay
        #else:
        #    self.epsilon = self.epsilon_max

    def train(self, env, episodes=30):

        count = 0
        self.initialize(len(self.get_position_list()),128)#16384
        
        for episode in range(episodes):

            #print((self.get_position_list()),2**9)
            #self.q_table = np.random.rand(2**9,len(self.get_position_list()))
            #self.epsilon = 1.0

            """if(episode%21 == 0):
                self.epsilon = self.epsilon_max"""

            done, truncated = False, False
            observation, info = env.reset()
            #print(info)
            current_state = self.get_current_state(observation)

            #obs1 = 0
            total_rew = 0

            while not done and not truncated:
                count += 1
                action = self.select_action(current_state)
                #print("action", action)
                new_observation, reward, done, truncated, info = env.step(action)
                next_state = self.get_current_state(new_observation)
                self.update_q_table(current_state, action, reward, next_state)
                current_state = next_state
                #self.update_epsilon()
                total_rew += reward
                """if(count < 50):
                    print("action: ", action, "reward: ", reward, "last position: ", new_observation[len(new_observation)-2], "real position: ", new_observation[len(new_observation)-1])"""
                    #print(" total_reward: ", total_rew, "reward ", reward)
                #print("relevant observations", new_observation[0], new_observation[1], new_observation[2], " state: ", current_state)
                #obs1 += new_observation[0]

                #print("total reward: ", reward, "calculated_observation ", np.log(new_observation[0]* self.get_position_list()[action]))

                """if(new_observation[0] < 0):
                    print(new_observation)
                    print("observation", new_observation[len(new_observation)-1], new_observation[len(new_observation)-2])
                    print("reward", reward)"""
                


    def get_test_position(self, observation, reward=0):
        state = self.get_current_state(observation)

        """if(observation[0] < 0):
            print(observation)
            print("observation", observation[len(observation)-1], observation[len(observation)-2])"""
        
        """if(observation[0] * observation[len(observation)-1] < 0):
            print(observation)
            #print(len(observation)-2)
            print(int(observation[len(observation)-2]))
            print(self.q_table[state])
            print(info)
            self.q_table[state][int(observation[len(observation)-2])] += 1
            self.q_table[state][int(1-observation[len(observation)-2])] -= 1"""



        #choose highet of qtable 0 and 2 index, excludint 1
        """if(self.q_table[state][0] > self.q_table[state][2]):
            action = 0
        elif(self.q_table[state][0] < self.q_table[state][2]):
            action = 2
        else:
            action = 1"""

        #print("state: ", state, " q_table: ", self.q_table[state], " action: ", action, " reward: ", reward)

        action = np.argmax(self.q_table[state])
        return action

    def test(self, env):
        # DO NOT CHANGE - all changes will be ignored after upload to BRUTE!
        done, truncated = False, False
        observation, info = env.reset()
        position = 0
        total_rew = 0
        reward = 0
        #self.q_table = self.q_table - np.mean(self.q_table, axis=0)
        while not done and not truncated:
            new_position = self.get_test_position(observation)

            """if(observation[0] <0 and reward > 0):
                print("observation", observation)
                print("reward", reward)"""
            
            
            
            observation, reward, done, truncated, info = env.step(new_position)
            #print(new_position, reward, observation[0])
            #total_rew += reward


        """for a in range(len(self.q_table)):
            #if q_values of row arent 0
            if(np.sum(self.q_table[a]) != 0):
                print(a, self.q_table[a])"""


            #print(a, self.q_table[a])
        #id like to average out the q_values in q_table in column, so that their average is 0

        #print(self.q_table)

